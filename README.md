# URL Shortener (Golang + Google Cloud Datastore)

This is a POC __URL Shorterner__ web application written in [Golang](https://golang.org/) and use [Google Cloud Datastore](https://cloud.google.com/datastore/) as a database.

The frontend is written briefly with [VueJS](https://vuejs.org/) and [Bulma](https://bulma.io/).

__DEMO:__ https://url.spicydog.org

## Configutation

Copy the sample configutation file at `/config/config.go.example` to  `/config/config.go` and read the comments in the file for more details.

## Installation

At the momement, you have to compile by yourself but edit the configuration first.