package api

import (
	"net/http"
	"gitlab.com/spicydog/url-shortener/data"
	"encoding/json"
	"math/rand"
	"time"
	"fmt"
	"gitlab.com/spicydog/url-shortener/config"
	"sync"
)

var uniqueUrlMutex = &sync.Mutex{}

func Handle(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
}

type PostRequestData struct {
	OriginUrl string
}

func HandleCreate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var input PostRequestData
	err := decoder.Decode(&input)
	if err != nil {
		panic(err)
	}

	keyLength := 6

	uniqueUrlMutex.Lock()

	key := ""
	for {
		key = generateUniqueString(keyLength)
		isDuplicated := len(data.ReadRecord(key)) > 0
		if ! isDuplicated {
			break
		}
	}
	result := createCustomUrl(key, input.OriginUrl)

	uniqueUrlMutex.Unlock()

	if result {
		fmt.Fprintf(w, `%s%s`, config.ApplicationUrl, key)
	} else {
		w.WriteHeader(500)
	}
}

func generateUniqueString(n int) string {
	rand.Seed(time.Now().UnixNano())
	letterRunes := []rune("abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func createCustomUrl(shortUrl, longUrl string) bool {
	record := data.Record{
		Origin: longUrl,
	}
	return data.CreateRecord(shortUrl, record)
}
