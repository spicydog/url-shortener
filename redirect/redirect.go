package redirect

import (
	"net/http"
	"gitlab.com/spicydog/url-shortener/data"
	"strings"
)

func Handle(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path[1:]

	if len(path) == 0 {
		// Root path redirect to app
		http.Redirect(w, r, "/app", 301)
		return
	}

	originUrl := readShortUrl(path)

	if len(originUrl) == 0 {
		w.WriteHeader(404)
		return
	}

	if ! strings.HasPrefix(originUrl, "http://") &&
		! strings.HasPrefix(originUrl, "https://") {
		originUrl = "http://" + originUrl
	}

	http.Redirect(w, r, originUrl, 301)
}

func readShortUrl(shortUrl string) string {
	return data.ReadRecord(shortUrl)
}
