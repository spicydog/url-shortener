package main

import (
	"net/http"
	"log"
	"gitlab.com/spicydog/url-shortener/api"
	"gitlab.com/spicydog/url-shortener/redirect"
)

func main() {

	http.HandleFunc("/api/", api.Handle)
	http.HandleFunc("/api/create/", api.HandleCreate)
	http.HandleFunc("/", redirect.Handle)

	http.Handle("/app/", http.StripPrefix("/app/", http.FileServer(http.Dir("./public"))))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
