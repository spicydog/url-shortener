package data

import (
	"cloud.google.com/go/datastore"
	"io/ioutil"
	"log"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	"context"
	"github.com/bluele/gcache"
	"gitlab.com/spicydog/url-shortener/config"
)

type Record struct {
	Key    *datastore.Key `datastore:"__key__"`
	Origin string         `datastore:"origin,noindex"`
}

var ctx context.Context
var client *datastore.Client
var urlCache gcache.Cache

func init() {
	data, err := ioutil.ReadFile(config.ServiceAccountFile)
	if err != nil {
		log.Fatal(err)
	}

	ctx = context.Background()
	creds, err := google.CredentialsFromJSON(ctx, data, "https://www.googleapis.com/auth/datastore")
	if err != nil {
		log.Fatal(err)
	}

	projID := creds.ProjectID
	client, err = datastore.NewClient(ctx, projID, option.WithCredentials(creds))

	if err != nil {
		log.Fatalf("Could not create datastore client: %v", err)
	}

	// LRU cache size of 1000
	urlCache = gcache.New(1000).LRU().Build()
}

func CreateRecord(keyName string, record Record) bool {
	key := datastore.NameKey("urls", keyName, nil)

	if _, err := client.Put(ctx, key, &record); err == nil {
		urlCache.Set(keyName, &record)
		return true
	} else {
		return false
	}
}

func ReadRecord(keyName string) string {

	record := &Record{}

	if r, err := urlCache.Get(keyName); err == nil {
		record = r.(*Record)
	} else {
		key := datastore.NameKey("urls", keyName, nil)
		client.Get(ctx, key, record)
		urlCache.Set(keyName, record)
	}

	return record.Origin

}
